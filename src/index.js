import ScreenShot from './components/ScreenShot'

export function install (Vue) {
  Vue.component('ScreenShot', ScreenShot)
}

export {
  ScreenShot
}

const plugin = {
  version: '0.1.1',
  install
}

export default plugin

let GlobalVue = null
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue
}
if (GlobalVue) {
  GlobalVue.use(plugin)
}
