const path = require('path')

module.exports = {
  title: 'Vue Screenshot',
  components: 'src/components/**/[A-Z]*.vue',
  usageMode: 'expand',
  styleguideDir: 'public',
  renderRootJsx: path.join(__dirname, '/styleguide.root.js')
}
