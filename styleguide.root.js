import Vue from 'vue'

Vue.config.productionTip = false

export default previewComponent => {
  return {
    render (createElement) {
      return createElement(
        'v-app', {
          props: { id: 'v-app' }
        },
        [createElement(previewComponent)]
      )
    }
  }
}
